﻿using System;
using System.Collections.Generic;
using System.Text;

interface Prototype
{
    Prototype Clone();
}
