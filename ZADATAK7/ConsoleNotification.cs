﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_ZADATAK7
{
    public enum Category { ERROR, ALERT, INFO }

    class ConsoleNotification : Prototype
    {
        public String Author { get; private set; }
        public String Title { get; private set; }
        public String Text { get; private set; }
        public DateTime Timestamp { get; private set; }
        public Category Level { get; private set; }
        public ConsoleColor Color { get; private set; }

        public ConsoleNotification(String author, String title, String text, DateTime time, Category level, ConsoleColor color)
        {
            this.Author = author;
            this.Title = title;
            this.Text = text;
            this.Timestamp = time;
            this.Level = level;
            this.Color = color;
        }
        public ConsoleNotification()
        {
            this.Author = "Default";
            this.Title = "Default";
            this.Text = "Default";
            this.Timestamp = DateTime.Now;
            this.Level = Category.ALERT;
            this.Color = ConsoleColor.DarkYellow;
        }

        public Prototype Clone()
        {
            return (Prototype)this.MemberwiseClone();
            //Ima li u konkretnom slučaju razlike između plitkog i dubokog kopiranja ?
            //kao i u prvom slucaju, nema razlike.
        }

    }
}
