﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_ZADATAK7
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager Notification_Manager = new NotificationManager();

            ConsoleNotification Console_Notification = new ConsoleNotification("ERROR's successor, Shigaraki Tomura is here!", "He is here with League of Villains!", "He is here to destroy you.", DateTime.Now, Category.ERROR, ConsoleColor.Red);
            Notification_Manager.Display(Console_Notification);

            Console_Notification = new ConsoleNotification("You are ALERT, All Might's successor!", "You're working hard to become the number one hero!", "You are here to protect the city.", DateTime.Now, Category.ALERT, ConsoleColor.Green);
            Notification_Manager.Display(Console_Notification);

            Console_Notification = new ConsoleNotification("INFORMATION is still a narrator.", "The battle between you and Tomura is coming to a close, both of you are on the verge of death.", "INFORMATON, the real mastermind behind everything, takes this opportunity to take over the world.", DateTime.Now, Category.INFO, ConsoleColor.DarkYellow);
            Notification_Manager.Display(Console_Notification);

            ConsoleNotification Cloned_Console_Notification = new ConsoleNotification();
            Notification_Manager.Display(Cloned_Console_Notification);

            Cloned_Console_Notification = (ConsoleNotification)Console_Notification.Clone();
            Notification_Manager.Display(Cloned_Console_Notification);

            Console.ReadKey();

        }
    }
}
