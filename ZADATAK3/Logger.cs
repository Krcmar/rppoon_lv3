﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_ZADATAK3
{
    class Logger
    {
        private static Logger instance;
        private string filePath;

        private Logger()
        {
            this.filePath = "log.txt";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }

            return instance;
        }

        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(message);
            }
        }
        public void SetLogPath(string newPath)
        {
            filePath = newPath;
        }
    }
}
//Ako je datoteka postavljena na jednom mjestu u tekstu programa, hoće li uporaba loggera na
//drugim mjesta u testu programa pisati u istu datoteku(pretpostavka je kako nisu ponovo postavljene)?

//Pisati ce u istu datoteku jer je putanja do datoteke jednaka. Jedino bi pisalo u drugu da napravimo novu putanju do te datoteke.
