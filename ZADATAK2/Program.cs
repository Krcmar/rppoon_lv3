﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_ZADATAK2
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixGenerator Matrix_Generator = MatrixGenerator.GetInstance();

            int[,] matrix = new int[5, 8];
            matrix = Matrix_Generator.GetMatrix(5, 8);

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(String.Format("{0}", matrix[i, j]) + "\t");
                }
                Console.Write("\n");
            }

            Console.ReadKey();
        }
    }
}
