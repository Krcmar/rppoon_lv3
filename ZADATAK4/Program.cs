﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_ZADATAK4
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager Notification_Manager = new NotificationManager();

            ConsoleNotification Console_Notification = new ConsoleNotification("ERROR has been initiated, Deku!", "You are facing a villain, infamous All For One!", "Watch out! He is toxic and dangerous.", DateTime.Now, Category.ERROR, ConsoleColor.Red);
            Notification_Manager.Display(Console_Notification);

            Console_Notification = new ConsoleNotification("ALERT comes to the rescue!", "He is the number one pro hero, All Might!", "ALERT uses error shield!", DateTime.Now, Category.ALERT, ConsoleColor.Blue);
            Notification_Manager.Display(Console_Notification);

            Console_Notification = new ConsoleNotification("INFORMATION is the narrator.", "The battle lasted for five centuries. You have continously been attacked and saved by two rivals, bringing the epic tale to its end.", "You have all died.", DateTime.Now, Category.INFO, ConsoleColor.DarkYellow);
            Notification_Manager.Display(Console_Notification);

            Console.ReadKey();
        }
    }
}
