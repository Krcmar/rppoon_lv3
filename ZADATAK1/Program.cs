﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("test.csv");
            Console.WriteLine("Originial file data: \n" + dataset.ToString());

            Dataset cloneDataset = new Dataset();
            cloneDataset = (Dataset)dataset.Clone();
            Console.WriteLine("Clone file data: \n" + cloneDataset.ToString());

            if (object.ReferenceEquals(dataset, cloneDataset))
                Console.WriteLine("Objects are the same.");
            else
                Console.WriteLine("Objects are not the same.");

            Console.ReadKey();
        }
    }

}
//Nije nuzno duboko kopiranje jer nema privatne članove kojima se pristupa izvana