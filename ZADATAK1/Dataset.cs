﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    class Dataset : Prototype
    {
    private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }

        public Prototype Clone()
        {
            List<List<string>> CloneList = new List<List<string>>();

            foreach (List<string> list in this.data)
            {
                CloneList.Add(new List<string>());
                foreach (string c in list)
                {
                    CloneList[CloneList.Count - 1].Add(c);
                }
            }
            Dataset NewestData = new Dataset
            {
                data = CloneList
            };

            return (Prototype)NewestData;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (List<string> list in this.data)
            {
                foreach (string c in list)
                {
                    stringBuilder.Append(c);
                }
                stringBuilder.Append("\n");
            }
            return stringBuilder.ToString();
        }



        public void ClearData()
        {
            this.data.Clear();
        }
    }
}