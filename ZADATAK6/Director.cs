﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_ZADATAK6
{
    class Director
    {
        private IBuilder builder;

        public void Construct_ERROR_Notification(string author)
        {
            builder.SetAuthor(author).SetColor(ConsoleColor.Red).SetLevel(Category.ERROR).
                SetText("Error notification").SetTime(DateTime.Now).SetTitle("ERROR");
        }

        public void Construct_ALERT_Notification(string author)
        {
            builder.SetAuthor(author).SetColor(ConsoleColor.Blue).SetLevel(Category.ALERT).
                SetText("Alert notification").SetTime(DateTime.Now).SetTitle("ALERT");
        }

        public void Construct_INFO_Notification(string author)
        {
            builder.SetAuthor(author).SetColor(ConsoleColor.DarkYellow).SetLevel(Category.INFO).
                SetText("Informational notification").SetTime(DateTime.Now).SetTitle("INFO");
        }

        public Director(IBuilder builder)
        {
            this.builder = builder;
        }

    }
}
