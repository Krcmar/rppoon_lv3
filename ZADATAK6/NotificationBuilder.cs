﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_ZADATAK6
{
    class NotificationBuilder : IBuilder
    {
        public String Author { get; private set; }
        public String Title { get; private set; }
        public String Text { get; private set; }
        public DateTime Timestamp { get; private set; }
        public Category Level { get; private set; }
        public ConsoleColor Color { get; private set; }

        public ConsoleNotification Build()
        {
            return new ConsoleNotification(Author, Title, Text, Timestamp, Level, Color);
        }

        public IBuilder SetAuthor(string author)
        {
            Author = author;
            return this;
        }

        public IBuilder SetTitle(string title)
        {
            Title = title;
            return this;
        }

        public IBuilder SetText(string text)
        {
            Text = text;
            return this;
        }

        public IBuilder SetTime(DateTime time)
        {
            Timestamp = time;
            return this;
        }

        public IBuilder SetLevel(Category level)
        {
            Level = level;
            return this;
        }

        public IBuilder SetColor(ConsoleColor color)
        {
            Color = color;
            return this;
        }
    }
}