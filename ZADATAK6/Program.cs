﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_ZADATAK6
{
    class Program
    {
        static void Main(string[] args)
        {

            NotificationBuilder Notification_Builder = new NotificationBuilder();
            NotificationManager Notification_Manager = new NotificationManager();
            Director director = new Director(Notification_Builder);

            director.Construct_ERROR_Notification("This author is an Error");
            Notification_Manager.Display(Notification_Builder.Build());

            director.Construct_ALERT_Notification("This author is an Alert");
            Notification_Manager.Display(Notification_Builder.Build());

            director.Construct_INFO_Notification("This author is an Information");
            Notification_Manager.Display(Notification_Builder.Build());

            Console.ReadKey();
        }
    }
}
